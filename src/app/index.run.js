(function() {
    'use strict';

    angular
        .module('ladislaogarcia-web')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {

        $log.debug('runBlock end');
    }

})();