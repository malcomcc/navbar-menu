;(function() {
    'use strict';

    angular
        .module('ladislaogarcia-web', [
          'ngAnimate',
          'ngCookies',
          'ngTouch',
          'ngSanitize',
          'ngResource',
          'ui.bootstrap',
          'ui.router',
          'lgNavbarComponent',
          'fullPage.js'
         ]);

})();
