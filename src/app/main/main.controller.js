/* globals $, window, toastr */
;(function() {
    'use strict';

    /** @ngInject */
    function MainController() {
      var vm = this;
    }
    MainController.$inject = [];

    angular
        .module('ladislaogarcia-web')
        .controller('MainController', MainController);
})();
