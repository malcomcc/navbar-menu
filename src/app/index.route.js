(function() {
    'use strict';

    angular
        .module('ladislaogarcia-web')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($locationProvider, $stateProvider, $urlRouterProvider) {
      $locationProvider.html5Mode(true);
      $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        params: {
          scrollto: null
        }
      })
      .state('two', {
        url: '/two',
        templateUrl: 'app/two/two.html',
        controller: 'TwoController'
      })
      .state('three', {
        url: '/three',
        templateUrl: 'app/three/three.html',
        controller: 'ThreeController'
      });
      $urlRouterProvider.otherwise('/');
    }

})();
