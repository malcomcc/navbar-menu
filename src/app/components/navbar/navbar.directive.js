;(function() {
  'use strict';

  function lgNavbar() {
    return {
      restrict: 'E',
      scope: {
        busqueda: '@'
      },
      templateUrl: 'app/components/navbar/navbar.html'
    };
  }

  angular
  .module('lgNavbarComponent')
  .directive('lgNavbar',lgNavbar);

})();
