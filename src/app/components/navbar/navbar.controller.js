/* globals window, $, toastr */
;(function() {
  'use strict';

  function lgNavbarController($location,$transitions,$scope,$state) {
    var vm = this;
    vm.routes = [
      {
        page: 'main',
        items: [
          {
            text: 'main1'
          },
          {
            text: 'main2'
          }
        ]
      },
      {
        page: 'two',
        items: [
          {
            text: 'two1'
          },
          {
            text: 'two2'
          }
        ]
      },
      {
        page: 'three'
      }
    ];
    vm.initialPage = $location.$$path.replace('/','') || vm.routes[0].page;
    vm.currentNavItem = vm.currentNavItem || vm.initialPage;
    vm.currentSubNavItem = vm.currentSubNavItem || vm.routes[0].items[0].text;
    vm.isOpened = vm.isOpened || false;

    $transitions.onSuccess( {}, function(trans) {
      vm.currentNavItem = angular.isDefined( $location.$$path ) && $location.$$path !== '' && $location.$$path !== '/' ? $location.$$path.replace('/','') : vm.routes[0].page;
      vm.hash = angular.isDefined( $location.$$hash ) && $location.$$hash !== '' ? $location.$$hash.replace('/','') : false;
      window.setTimeout( function() {
        if( vm.hash ) {
          $('html,body').animate({
            scrollTop: $('#'+vm.hash).length >= 1 ? $('#'+vm.hash).position().top : 0+'px'
          },700);
        }
      },50);
    });

    vm.navigateTo = function(params) {
      console.log( $location );
      if( !vm.isObject(params) ) {
        throw 'Parameter is not an object.';
      }
      vm.navigate = params.page && params.page.trim() !== '' && params.page !== vm.currentNavItem;
      if( vm.navigate ) {
        $state.go(params.page);
        vm.currentNavItem = params.page;
        if( $('#submenu').find('.'+vm.currentNavItem).length >= 1 ) {
          vm.manageSubmenu(vm.currentNavItem);
        }
      }
    };

    vm.manageSubmenu = function( menu ) {
      if( !menu ) {
        throw 'Falta el parámetro submenu';
      }
      vm.currentSubNavItem = submenu;
      if( vm.hasSubmenu(menu) ) {
        vm.moveSubmenu( parseInt($('#submenu').height()), menu );
      } else {
        vm.currentSubNavItem = null;
        vm.moveSubmenu();
      }
    };

    vm.hasSubmenu = function(option) {
      return document.querySelector('#submenu').querySelectorAll('.'+option).length >= 1;
    };

    vm.moveSubmenu = function( to, submenu ) {
      to = to || 0;
      vm.isOpened = to !== 0;
      $('#submenu').animate({
        'top': - to+'px'
      },1700, function() {
        if( submenu ) {
          vm.currentSubNavItem = submenu;
        }
      });
    };

    vm.isInteger = function( item ) {
      return parseInt(item) || item === 0;
    }

    vm.isObject = function(value) {
      return Object(value) === value;
    };

  }
  lgNavbarController.$inject = ['$location','$transitions','$scope','$state'];

  angular.module('lgNavbarComponent')
  .controller('lgNavbarController', lgNavbarController);

})();
